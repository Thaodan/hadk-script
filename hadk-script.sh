#!/usr/bin/bash

MERSDK_CHROOT_TAR=mer-i486-latest-sdk-rolling-chroot-armv7hl-sb2.tar.bz2
UBUNTU_ROOTFS_TAR=ubuntu-trusty-android-rootfs.tar.bz2

CONFDIR="${XDG_CONFIG_HOME-:$HOME/.config}/hadk"
CACHE_DIR=${XDG_CACHE_HOME:-$HOME/.cache}
verbose()
{
    [ $be_chatty ]  &&   echo "$@"
}

warn()
{
    echo "$@" >&2
}

die()
{
    warn "$1"
    exit ${2:-1}
}

mer_sdk_run()
{
    echo stub
    echo "$@"
}

init_conf()
{
    # only init conf if not already done
    mkdir $CONFDIR
    if [ ! -e $CONFDIR/env ] ; then
        cat <<EOF > $CONFDIR/env
export MER_ROOT="$HOME/mer"
export ANDROID_ROOT="$MER_ROOT/android/droid"
export VENDOR="sony"
export DEVICE="scorpion"
export PORT_ARCH="armv7hl"
EOF
    fi

    if [ ! -e $CONFDIR/mersdkubu.profile ] ; then
        cat <<EOF >> $CONFDIR/mersdkubu.profile
function hadk() { source $HOME/.hadk.env; echo "Env setup for $DEVICE"; }
export PS1="HABUILD_SDK [\${DEVICE}] $PS1"
hadk
EOF
    fi

    if [ ! -e "$CONFDIR/mersdk.profile" ] ; then
        cat <<EOF >> $CONFDIR/.mersdk.profile
function hadk() { source $HOME/.hadk.env; echo "Env. setup for $DEVICE"; }
hadk
EOF
    fi
}

mkdir $CACHE_DIR
curl -k -o https://img.merproject.org/images/mer-sdk/$MERSDK_CHROOT_TAR $CACHE_DIR/$MERSDK_CHROOT_TAR
mkdir -p $MER_ROOT/sdks/sdk
cd $MER_ROOT/sdks/sdk
tar --numeric-owner -p -xjf $CACHE_DIR/$MERSDK_CHROOT_TAR

verbose "Adding the curlfix repo"
mer_sdk_run 'sudo zypper ar http://repo.merproject.org/obs/home:/sledge:/mer/latest_i486/ \
     curlfix'
verbose "Adding the common repo"
mer_sdk_run 'sudo zypper ar http://repo.merproject.org/obs/nemo:/devel:/hw:/common/sailfish_latest_armv7hl/ \
     common' 
verbose "Refreshing the curlfix repo"
mer_sdk_run 'sudo zypper ref curlfix;sudo zypper dup --from curlfix'
verbose "Refreshing the common repo"
mer_sdk_run 'sudo zypper ref common sudo zypper dup --from common'
verbose "Installing android-tools createrepo and zip"
mer_sdk_run 'sudo zypper in android-tools createrepo zip'
verbose "Downloading the Rootfs tarball for Android"
curl -o http://img.merproject.org/images/mer-hybris/ubu/$UBUNTU_ROOTFS_TAR $CACHE_DIR/$UBUNTU_ROOTFS_TAR

UBUNTU_CHROOT=$MER_ROOT/sdks/ubuntu
mkdir -p $UBUNTU_CHROOT
verbose "Unpacking the Rootfs tarball for Android into $UBUNTU_CHROOT"
tar --numeric-owner -xvjf $CACHE_DIR/$UBUNTU_ROOTFS_TAR -C $UBUNTU_CHROOT
verbose "Entering the HABUILD_SDK"
ubu-chroot -r $MER_ROOT/sdks/ubuntu
verbose "Exiting the HABUILD_SDK"

git config --global user.name "Your name here please"
git config --global user.email "Your emailadress here please"

if ! which repo > /dev/null ; then
    die 'you need to install repo'
fi
ubu-chroot -r $MER_ROOT/sdks/ubuntu
verbose Creating the Root directory for Android
sudo mkdir -p $ANDROID_ROOT
verbose Changing owner to the root directory for Android to your user
sudo chown -R $USER $ANDROID_ROOT
verbose Entering the root directory for Android
cd $ANDROID_ROOT
verbose Initialising the hybris repo into $ANDROID_ROOT
repo init -u git://github.com/mer-hybris/android.git -b hybris-12.1
verbose Creating the local_manifests directory
mkdir $ANDROID_ROOT/.repo/local_manifests
verbose Entering the local_manifests directory
cd $HOME/.repo/local_manifests/
verbose Creating the scorpion.xml file
touch scorpion.xml
verbose Putting relevant repos to sync in the scorpion.xml file
cat > scorpion.xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<manifest>
<project path="device/sony/scorpion" name="CyanogenMod/android_device_sony_scorpion" revision="cm-12.1" />
<project path="device/sony/scorpion_windy" name="CyanogenMod/android_device_sony_scorpion_windy" revision="cm-12.1" />
<project path="device/sony/shinano-common" name="CyanogenMod/android_device_sony_shinano-common" revision="cm-12.1" />
<project path="device/sony/msm8974-common" name="CyanogenMod/android_device_sony_msm8974-common" revision="cm-12.1" />
<project path="device/sony/common" name="CyanogenMod/android_device_sony_common" revision="cm-12.1" />
<project path="device/qcom/common" name="CyanogenMod/android_device_qcom_common" revision="cm-12.1" />
<project path="hardware/sony/thermanager" name="CyanogenMod/android_hardware_sony_thermanager" revision="cm-12.1" />
<project path="kernel/sony/msm8974" name="Nokius/android_kernel_sony_msm8974" revision="hybris-12.1" />
<project path="vendor/sony/" name="TheMuppets/proprietary_vendor_sony" revision="cm-12.1" />
<project path="rpm/" name="Nokius/droid-hal-scorpion" revision="master" />
<project path="hybris/droid-configs" name="Nokius/droid-config-scorpion" revision="master" />
<project path="hybris/droid-hal-version-scorpion" name="Nokius/droid-hal-version-scorpion" revision="master" />
</manifest>
EOF
verbose Entering $ANDROID_ROOT
cd $ANDROID_ROOT
verbose "Syncing the repos that you added to the scorpion.xml file"
repo sync --fetch-submodules
verbose "Resyncing again to make sure that you're fully up-to-date."
repo sync --fetch-submodules
cat > $MER_ROOT/android/droid/hybris/hybris-boot/fixup-mountpoints << EOF
#!/bin/bash

DEVICE=$1
shift

echo "Fixing mount-points for device $DEVICE"

case "$DEVICE" in
    
    "scorpion")
         sed -i \
             -e 's block/platform/msm_sdcc.1/by-name/B2B mmcblk0p25 ' \
             -e 's block/platform/msm_sdcc.1/by-name/DDR mmcblk0p17 ' \
             -e 's block/platform/msm_sdcc.1/by-name/FOTAKernel mmcblk0p16 ' \
             -e 's block/platform/msm_sdcc.1/by-name/LTALabel mmcblk0p18 ' \
             -e 's block/platform/msm_sdcc.1/by-name/TA mmcblk0p1 ' \
             -e 's block/platform/msm_sdcc.1/by-name/aboot mmcblk0p5 ' \
             -e 's block/platform/msm_sdcc.1/by-name/alt_aboot mmcblk0p11 ' \
             -e 's block/platform/msm_sdcc.1/by-name/alt_dbi mmcblk0p10 ' \
             -e 's block/platform/msm_sdcc.1/by-name/alt_rpm mmcblk0p12 ' \
             -e 's block/platform/msm_sdcc.1/by-name/alt_s1sbl mmcblk0p9 ' \
             -e 's block/platform/msm_sdcc.1/by-name/alt_sbl1 mmcblk0p8 ' \
             -e 's block/platform/msm_sdcc.1/by-name/alt_tz mmcblk0p13 ' \
             -e 's block/platform/msm_sdcc.1/by-name/apps_log mmcblk0p22 ' \
             -e 's block/platform/msm_sdcc.1/by-name/boot mmcblk0p14 ' \
             -e 's block/platform/msm_sdcc.1/by-name/cache mmcblk0p24 ' \
             -e 's block/platform/msm_sdcc.1/by-name/dbi mmcblk0p4 ' \
             -e 's block/platform/msm_sdcc.1/by-name/fsg mmcblk0p21 ' \
             -e 's block/platform/msm_sdcc.1/by-name/modemst1 mmcblk0p19 ' \
             -e 's block/platform/msm_sdcc.1/by-name/modemst2 mmcblk0p20 ' \
             -e 's block/platform/msm_sdcc.1/by-name/ramdump mmcblk0p15 ' \
             -e 's block/platform/msm_sdcc.1/by-name/rpm mmcblk0p6 ' \
             -e 's block/platform/msm_sdcc.1/by-name/s1sbl mmcblk0p3 ' \
             -e 's block/platform/msm_sdcc.1/by-name/sbl1 mmcblk0p2 ' \
             -e 's block/platform/msm_sdcc.1/by-name/system mmcblk0p23 ' \
             -e 's block/platform/msm_sdcc.1/by-name/tz mmcblk0p7 ' \
             -e 's block/platform/msm_sdcc.1/by-name/userdata mmcblk0p26 ' \
            "$@" ;;

     *)
        exit 1
        ;;
esac
EOF
echo - Done!
verbose Running source build/envsetup.sh
source build/envsetup.sh
export USE_CCACHE=1
verbose Running breakfast for the device you told me to build for
breakfast $DEVICE
verbose Running make to build hybris-hal
make -j4 hybris-hal
verbose Running java -jar $HOME/mer/android/droid/out/host/linux-x86/framework/dumpkey.jar build/target/product/security/testkey.x509.pem build/target/product/security/cm.x509.pem build/target/product/security/cm-devkey.x509.pem > $HOME/mer/android/droid/out/target/product/$DEVICE/obj/PACKAGING/ota_keys_intermediates/keys in order for make to complete successfully
java -jar $HOME/mer/android/droid/out/host/linux-x86/framework/dumpkey.jar build/target/product/security/testkey.x509.pem build/target/product/security/cm.x509.pem build/target/product/security/cm-devkey.x509.pem > $HOME/mer/android/droid/out/target/product/scorpion/obj/PACKAGING/ota_keys_intermediates/keys
verbose Done!
verbose Rerunning make to build hybris-hal once again
make -j4 hybris-hal
verbose Showing the kernel config file built by hybris
hybris/mer-kernel-check/mer_verify_kernel_config \
    ./out/target/product/$DEVICE/obj/KERNEL_OBJ/.config
verbose Building hybris-boot and hybris-recovery for your device
make hybris-boot && make hybris-recovery
verbose Exiting the HABUILD_SDK into the Mer SDK
exit
SFE_SB2_TARGET=$MER_ROOT/targets/$VENDOR-$DEVICE-$PORT_ARCH
TARBALL_URL=http://releases.sailfishos.org/sdk/latest/targets/targets.json
TARBALL=$(curl $TARBALL_URL | grep "$PORT_ARCH.tar.bz2" | cut -d\" -f4)
curl -O $TARBALL
sudo mkdir -p $SFE_SB2_TARGET
sudo tar --numeric-owner -pxjf $(basename $TARBALL) -C $SFE_SB2_TARGET
sudo chown -R $USER $SFE_SB2_TARGET
cd $SFE_SB2_TARGET
grep :$(id -u): /etc/passwd >> etc/passwd
grep :$(id -g): /etc/group >> etc/group
sb2-init -d -L "--sysroot=/" -C "--sysroot=/" \
         -c /usr/bin/qemu-arm-dynamic -m sdk-build \
         -n -N -t / $VENDOR-$DEVICE-$PORT_ARCH \
         /opt/cross/bin/$PORT_ARCH-meego-linux-gnueabi-gcc
verbose Rebuilding the rpm database
sb2 -t $VENDOR-$DEVICE-$PORT_ARCH -m sdk-install -R rpm --rebuilddb
sb2 -t $VENDOR-$DEVICE-$PORT_ARCH -m sdk-install -R zypper ar \
    -G http://repo.merproject.org/releases/mer-tools/rolling/builds/$PORT_ARCH/packages/ \
    mer-tools-rolling
sb2 -t $VENDOR-$DEVICE-$PORT_ARCH -m sdk-install -R zypper ref --force
cd $HOME
verbose Creating the Hello World-script...
cat > main.c << EOF
#include <stdlib.h>
#include <stdio.h>
int main(void) {
printf("Hello, world!\n");
return EXIT_SUCCESS;
}
EOF
verbose Compiling the Hello World script
sb2 -t $VENDOR-$DEVICE-$PORT_ARCH gcc main.c -o test
verbose "Running the Hello World script which should show \"Hello World!\""
sb2 -t $VENDOR-$DEVICE-$PORT_ARCH ./test
sudo zypper ref; sudo zypper dup
cd $HOME
sudo mkdir -p $MER_ROOT/devel
sudo chown -R $USER mer/devel
sb2 -t $VENDOR-$DEVICE-$PORT_ARCH -R -m sdk-install ssu ar common http://repo.merproject.org/obs/nemo:/devel:/hw:/common/sailfish_latest_armv7hl/
cd $ANDROID_ROOT
verbose Running the build_packages script
rpm/dhd/helpers/build_packages.sh
verbose "Press Ctrl C to stop this process if you at some point end up with what looks as a freeze and rerun rpm/dhd/helpers/build_packages.sh"
echo Creating a directory called tmp
mkdir -p tmp
HA_REPO="repo --name=adaptation0-$DEVICE-@RELEASE@"
KS="Jolla-@RELEASE@-$DEVICE-@ARCH@.ks"
sed -e "s|^$HA_REPO.*$|$HA_REPO --baseurl=file://$ANDROID_ROOT/droid-local-repo/$DEVICE|" $ANDROID_ROOT/hybris/droid-configs/installroot/usr/share/kickstarts/$KS > tmp/$KS
RELEASE=2.0.1.11
EXTRA_NAME=-my1
sudo mic create fs --arch $PORT_ARCH \
     --debug \
     --tokenmap=ARCH:$PORT_ARCH,RELEASE:$RELEASE,EXTRA_NAME:$EXTRA_NAME \
     --record-pkgs=name,url \
     --outdir=sfe-$DEVICE-$RELEASE$EXTRA_NAME \
     --pack-to=sfe-$DEVICE-$RELEASE$EXTRA_NAME.tar.bz2 \
     $ANDROID_ROOT/tmp/Jolla-@RELEASE@-$DEVICE-@ARCH@.ks




